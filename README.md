#Contract
Based on: https://github.com/csswizardry/Contract

##Modifications
I've implemented a grunt-focused workflow with a markdown renderer and a mustache templating engine.  

The sections of the contract have been made into mustache partials and live in ````src/partials````.

You may configure ````src/data.json```` to specify your name, the client's name, the project name, or any other data you may want.
