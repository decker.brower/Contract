module.exports = function(grunt) {
  require('matchdep').filter('grunt-*').forEach(grunt.loadNpmTasks);
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    //tasks...
    watch: {
      mustache: {
        files: 'src/**/*.mustache',
        tasks: ['renderMustache'],
      },
      data: {
        files: 'src/data.json',
        tasks: ['renderMustache'],
        options: {
          livereload: false
        }       
      },
      markdown: {
        files: 'src/tmp/contract.md',
        tasks: ['renderMarkdown']
      },
      options: {
        livereload: true
      }
    },
    connect: {
      server: {
        options: {
          base: "build/",
          livereload: true,
          // open: true
        }
      }
    },
    clean: {
      tmp: ["src/tmp/"],
      build: ["build/"],
    },
    markdown: {
      contract: {
        files: [
          {
            expand: true,
            cwd: 'src/tmp',
            src: 'contract.md',
            dest: 'build',
            ext: '.html'
          }
        ],
      },
      options: {
        template: 'src/template.html',
        markdownOptions: {
          gfm: true,
          breaks: true
        }
      }
    },
    mustache_render: {
      contract: {
        files: [{
          data: 'src/data.json',
          template: 'src/contract.mustache',
          dest: 'src/tmp/contract.md'
        }]
      },
      options: {
        directory: 'src/partials'
      }
    }
  });
  grunt.registerTask( 'default', ['dev'] );

  grunt.registerTask( 'dev', ['clean', 'renderMustache', 'renderMarkdown', 'connect', 'watch' ] );

  grunt.registerTask( 'renderMustache', [ 'mustache_render' ] );
  grunt.registerTask( 'renderMarkdown', [ 'markdown' ] );
  
};